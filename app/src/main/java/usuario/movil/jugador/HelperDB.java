package usuario.movil.jugador;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class HelperDB extends SQLiteOpenHelper {

    int c = 0;
    private static final String TABLE_CONTROL_DATOS =
            "CREATE TABLE  parametros (cedula text, nombre text, apellidos text, ciudad text,  fecha text, equipo text, calculo int)";

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "MiBasedeDatos.db";


    public HelperDB(Context context) {
        super(context, DATABASE_NAME, null,  DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CONTROL_DATOS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS parametros");
        onCreate(db);

    }


    public String LeerTodo() {

        String consulta = "";
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM parametros", null);
        if (cursor.moveToFirst()){
            do{


                String EstuCedula = cursor.getString(cursor.getColumnIndex("cedula"));
                String EstuNombre = cursor.getString(cursor.getColumnIndex("nombre"));
                String EstuApellidos = cursor.getString(cursor.getColumnIndex("apellidos"));
                String EstuCiudad = cursor.getString(cursor.getColumnIndex("ciudad"));
                String EstuFecha = cursor.getString(cursor.getColumnIndex("fecha"));
                String EstuEquipo = cursor.getString(cursor.getColumnIndex("equipo"));
                String EstuCalculo = cursor.getString(cursor.getColumnIndex("calculo"));
                String dat = ("Cumple con los requisitos");

                consulta += "Cedula : " + EstuCedula + "\n" + "Nombre : " + EstuNombre + "\n" + "Apellidos : " + EstuApellidos + "\n" + "Ciudad : " + EstuCiudad + "\n"  + "Fecha de Nacimiento : " + EstuFecha +  "\n" + "Equipo debutante : " + EstuEquipo  +  "\n" + "Edad : " + EstuCalculo + " años" +  "\n"  + dat  +  "\n" +  "\n" +  "\n";
            }while (cursor.moveToNext());
        }
        return consulta;
    }
}
