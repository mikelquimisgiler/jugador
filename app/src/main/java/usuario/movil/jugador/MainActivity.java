package usuario.movil.jugador;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

    EditText et_cedula, et_nombre, et_apellidos,et_ciudad,  et_fecha, et_equipo, calculo;
    Button button_guardar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        et_cedula = (EditText)findViewById(R.id.et_cedula);
        et_nombre = (EditText)findViewById(R.id.et_nombre);
        et_apellidos = (EditText)findViewById(R.id.et_apellidos);
        et_ciudad = (EditText)findViewById(R.id.et_ciudad);
        et_fecha = (EditText)findViewById(R.id.et_fecha);
        et_equipo = (EditText)findViewById(R.id.et_equipo);
        calculo = (EditText)findViewById(R.id.calculo);
        button_guardar = (Button)findViewById(R.id.btnguardar);



        final HelperDB helperbd = new HelperDB(getApplicationContext());




        button_guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = et_cedula.getText().toString();
                if (TextUtils.isEmpty(name)){
                    et_cedula.setError("Complete el campo");
                    et_cedula.requestFocus();
                    return;
                }

                String name2 = et_nombre.getText().toString();
                if (TextUtils.isEmpty(name2)){
                    et_nombre.setError("Complete el campo");
                    et_nombre.requestFocus();
                    return ;
                }
                String name4 = et_apellidos.getText().toString();
                if (TextUtils.isEmpty(name4)){
                    et_apellidos.setError("Complete el campo");
                    et_apellidos.requestFocus();
                    return ;

                }
                String name1 = et_ciudad.getText().toString();
                if (TextUtils.isEmpty(name1)){
                    et_ciudad.setError("Complete el campo");
                    et_ciudad.requestFocus();
                    return;
                }

                String name3 = et_fecha.getText().toString();
                if (TextUtils.isEmpty(name3)){
                    et_fecha.setError("Complete el campo");
                    et_fecha.requestFocus();
                    return ;
                }
                String name6 = et_equipo.getText().toString();
                if (TextUtils.isEmpty(name6)){
                    et_equipo.setError("Complete el campo");
                    et_equipo.requestFocus();
                    return;
                }

                String name8 = calculo.getText().toString();
                if (TextUtils.isEmpty(name8)){
                    calculo.setError("Complete el campo");
                    calculo.requestFocus();
                    return ;
                }


                float calculoedad = Float.parseFloat(calculo.getText().toString());
                if(calculoedad >= 18){

                    SQLiteDatabase db = helperbd.getWritableDatabase();
                    ContentValues valores = new ContentValues();
                    valores.put("cedula",et_cedula.getText().toString());
                    valores.put("nombre", et_nombre.getText().toString());
                    valores.put("apellidos", et_apellidos.getText().toString());
                    valores.put("ciudad", et_ciudad.getText().toString());
                    valores.put("fecha", et_fecha.getText().toString());
                    valores.put("equipo", et_equipo.getText().toString());
                    valores.put("calculo", calculo.getText().toString());


                    Long IdGuardado = db.insert("parametros", "cedula",  valores);
                    Toast.makeText(getApplicationContext(),
                            "Se guardo el registro a la base de datos: ",
                            Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this,principal.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                }else{
                    Toast.makeText(getApplicationContext(),
                            "No se pueden registrar los datos, no cumple con los requisitos : ",
                            Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainActivity.this,principal.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }}
        });


    }

}
